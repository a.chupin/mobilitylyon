# Prérequis :

Installer les éléments suivant :


* Node JS version 12.8 ou +
* MongoDB
* Python vesrion 3
* Jupyter notebook
* noyau dotnet-try





# Command line instructions

You can also upload existing files from your computer using the instructions below.`


## Git global setup

~~~~
git config --global user.name "[your name]"
git config --global user.email "[your email]"
~~~~

## Create a new repository

~~~~
git clone https://gitlab.com/a.chupin/mobilitylyon.git
cd mobilitylyon
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
~~~~


## Push an existing folder

~~~~
cd existing_folder
git init
git remote add origin https://gitlab.com/a.chupin/mobilitylyon.git
git add .
git commit -m "Initial commit"
git push -u origin master
~~~~


## Push an existing Git repository

~~~~
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/a.chupin/mobilitylyon.git
git push -u origin --all
git push -u origin --tags
~~~~